import {useState, useEffect} from 'react';

export default () => {
  // the first element right here is the value of that state property.
  // the second is a function that we can use to change that value.
  const [lat, setLat] = useState(null);
  const [errorMessage, setErrorMessage] = useState('');

  // only want to have use effect to be invoked exactly one time when our component is 
  // first rendered. So use an empty array
  useEffect(() => {
    window.navigator.geolocation.getCurrentPosition(
      position => setLat(position.coords.latitude ),
      err => setErrorMessage(err.message )
    );
  },[]);

  return [lat, errorMessage];
};